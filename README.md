**SLB 3 - Study year 2020/2021**  
Authors: E. Hoekstra 
Date: 28th of June 2021   
Version: 1
Bio-informatics year 3  

### Who am I?

My name is Eric Hoekstra and I'm a 3th year student of bio-informatics at the HanzeHogeSchool in Groningen. For my minor period I chose the minor High  
Troughput and High performance Biocomputing. My interest in machine learning was my motivation to choose for this minor. In this minor period I worked on a minor project
related to neurology. I analysed neurological functional MRI images, we first preprocessed the MRI images and then we applied two types of learning on it to find
brain activity regions in it. After the statistical analysis was done we actually found some very interesting regions in the brain corresponding to popular brain regions.
This activated my interest in neurology.

I am a very ambitious and motivated student who is always looking for a great challenge. For my internship in September I will be working on neuropathology data in the
university medical centrum groningen (UMCG) in the molecular neurobiology sector, I'm very motivated to start working there.

There are a few programming languages which I'm very interested in, my best and most-used language is Python. I've now been using Python for 3 years. Next to that I have
experience in statistical programming and data analysis with R. My least favourite language is Java, I do have some experience in Java but I do not prefer it. Next
to that I do have experience in HTML, javascript and CSS and a pipeline programming language called Snakemake (python-like).

### Project links
Brain imaging analysis tool:
https://bitbucket.org/erichoekstra/brainnetworks_eric_linda/src/master/

Snakemake pipeline for performing fastQC on multiple fasta files.
https://bitbucket.org/erichoekstra/dataprocessing_eric_hoekstra/src/master/

Another project I did was creating a interactive covid-19 infection map. Each dot represents a country (in the Netherlands we made it province wide) and it will colour
itself depending on the amount of infections per 100.000 inhabitants.

![alt text](covid_map.png)


### Working experience

I've been working at Albert Heijn for the last 5 years. I started at Albert Heijn as a stock filling employee. After a few months I also started working as a cashier. 
After 3 years of working there I started my training for teamleader. After 5 months I had my final job interview and shortly after I was given the role of Teamleader. 
As a teamleader I'm fully responsible for the entire store when my store manager is not there (about 70% of the time). I also conduct job interviews with applicants.
This job offered me a lot of experience in having responsibilities and leading a team succesfully and respectfully.

### Contact

E-mail: e.j.hoekstra@bioinf.nl
Linkedin: https://www.linkedin.com/in/eric-hoekstra-4a673a145/

